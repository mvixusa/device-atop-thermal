#ifndef __THERMAL__
#define __THERMAL__

typedef long long int count_t;

#define MAXTEMPZN 64

extern int nr_thermal_zones;

struct pertemp {
    char name[13];
    char type[13];
	int temp;
	int slope;
	int offset;
};

struct tempstat {
	int nrzones;
	struct pertemp zone[MAXTEMPZN];
};

int init_thermal(void);
void photosyst_thermal(struct tempstat *);
	
#endif
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include "thermal.h"

#define TZPATH "/sys/class/thermal/"

#define THERMAL_NAME_LEN 13  /* including null terminator */
#define THERMAL_TYPE_LEN 13  /* including null terminator */

int nr_thermal_zones = 0;
int thermal_dirfd = -1;

char thermal_names[THERMAL_NAME_LEN * MAXTEMPZN];
char thermal_types[THERMAL_TYPE_LEN * MAXTEMPZN];

static int read_proc_str(int zone_dirfd, const char *filename, char *buf, size_t n, const char *defstr)
{
    int fd = openat(zone_dirfd, filename, O_RDONLY);
    ssize_t readcnt = read(fd, buf, n-1);
    close(fd);
    if (readcnt == -1) {
        strncpy(buf, defstr, n);
    } else {
        char *end = buf + readcnt - 1;
        if (*end != '\n') ++end;
        *end = '\0';
    }
    return readcnt;
}

static int read_proc_int(int zone_dirfd, const char *filename)
{
    int fd = openat(zone_dirfd, filename, O_RDONLY);
    FILE *fp = fdopen(fd, "r");
    if (fp == NULL)
        return -1;

    int value;
    int scanf_result = fscanf(fp, "%d", &value);
    if (scanf_result == 0 || scanf_result == EOF)
        value = -1;
    
    fclose(fp);
    return value;
}

int init_thermal()
{
    DIR *thermal = opendir(TZPATH);
    if (thermal != NULL) {
        struct dirent *entry;
        while ((entry = readdir(thermal))) {  /* single '=' is intentional */
            if (strncmp(entry->d_name, "thermal_zone", 12) == 0) {
                int num;
                int scanf_result = sscanf(entry->d_name+12, "%d", &num);
                if (scanf_result > 0 && scanf_result != EOF) {
                    if (num >= nr_thermal_zones)
                        nr_thermal_zones = num + 1;
                }
            }
        }
    }
    closedir(thermal);

    if (nr_thermal_zones > 0) {
        thermal_dirfd = open(TZPATH, O_RDONLY | O_PATH);
        if (thermal_dirfd == -1) {
            perror(TZPATH);
            return -1;
        }
    }

    char *thermal_name = thermal_names;
    char *thermal_type = thermal_types;
    int i; for (i=0; i<nr_thermal_zones; ++i) {
        char zonedir[32];
        snprintf(zonedir, 32, "thermal_zone%d", i);
        snprintf(thermal_names + THERMAL_NAME_LEN * i, THERMAL_NAME_LEN, "zone #%d", i);
        int dirfd = openat(thermal_dirfd, zonedir, O_RDONLY | O_PATH);
        read_proc_str(dirfd, "type", thermal_type, THERMAL_TYPE_LEN, "unknown type");
        close(dirfd);
        thermal_name += THERMAL_NAME_LEN;
        thermal_type += THERMAL_TYPE_LEN;
    }

    return nr_thermal_zones;
}

void photosyst_thermal(struct tempstat *tstat)
{
    tstat->nrzones = nr_thermal_zones;
    int i; for (i=0; i<nr_thermal_zones; ++i) {
        char zonedir[32];
        snprintf(zonedir, 32, "thermal_zone%d", i);
        int dirfd = openat(thermal_dirfd, zonedir, O_RDONLY | O_PATH);
        strncpy(tstat->zone[i].name, thermal_names + THERMAL_NAME_LEN * i, THERMAL_NAME_LEN);
        strncpy(tstat->zone[i].type, thermal_types + THERMAL_TYPE_LEN * i, THERMAL_TYPE_LEN);
        tstat->zone[i].temp = read_proc_int(dirfd, "temp");
        tstat->zone[i].slope = read_proc_int(dirfd, "slope");
        tstat->zone[i].offset = read_proc_int(dirfd, "offset");
        close(dirfd);
    }
}